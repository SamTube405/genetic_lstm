## source activate pnnl_socialsim
## python run_simulation_prob_branching_v1.py --config ./metadata/configs/twitter_cve_S2.json
import numpy as np
import pandas as pd
import sys,os
from random import choices
import random
from datetime import datetime as dt
import json
from ast import literal_eval
from pandas.io.json import json_normalize

import argparse
import logging
import logging.config
import json
import shutil
import os
import shutil
from os import listdir
from os.path import isfile, join

from libs.lib_simX_branching_v1 import *

# logger = logging.getLogger(__name__)
# logger.setLevel(logging.INFO)
# logging.config.dictConfig({
#     'version': 1,
#     'disable_existing_loggers': False,  # this fixes the problem
# })



from joblib import Parallel, delayed




def del_dir(x_dir):
    if(os.path.exists(x_dir)==True):
        try:
            shutil.rmtree(x_dir)
            print("Deleted. %s"%x_dir)
        except OSError as e:
            print("Error: %s - %s." % (e.filename, e.strerror))
            
def create_dir(x_dir):
    if not os.path.exists(x_dir):
        os.makedirs(x_dir)
        print("Created new dir. %s"%x_dir)


def reset_dir(x_dir):
    del_dir(x_dir)
    create_dir(x_dir)

def _get_input_posts(path):
    input_posts_lines=[]
    json_file=open(path,"r")
    for line in json_file.readlines():
        doc=json.loads(line)
        doc = {
                "id_h":doc["nodeID"],
                "author_h":doc["nodeUserID"],
                "created_date":doc["nodeTime"],
                "informationID":doc["informationID"],
                "communityID":doc["communityID"]
                }
        
        input_posts_lines.append(doc)
        
    input_posts_records=json_normalize(input_posts_lines)
    input_posts_records.set_index(pd.DatetimeIndex(input_posts_records['created_date']),inplace=True)
    return input_posts_records

def _run(args):
    version=args[0]
    
    simX=SimX(platform,actionType,domain,scenario,version_tag)
    simX.set_metadata(data_level_degree_list,data_delay_level_degree_root_list)
    simX.set_user_metadata(data_user)
    ##simX.doSanity()
    
    print("[started][job] %d"%version)
    simX._run_simulate(iposts_records,version)
    print("[completed][job] %d"%version)


"""
Load the simulation parameters
"""
parser = argparse.ArgumentParser(description='Simulation Parameters')
parser.add_argument('--config', dest='config_file_path', type=argparse.FileType('r'))
args = parser.parse_args()

config_json=json.load(args.config_file_path)
platform = config_json["PLATFORM"]
actionType = config_json["ACTION_TYPE"]
domain = config_json["DOMAIN"]
scenario = int(config_json["SCENARIO"])
no_trials = int(config_json["NO_TRIALS"])
version_tag = config_json["VERSION_TAG"]
input_posts_file_location=config_json["INPUT_SEEDS_FILE_PATH"]
#########


# ## loggers
# logPath='./logs/run_simulation_prob_HYDRA_%s_%s_%s.log'%(platform,domain,version_tag)
# handler = logging.FileHandler(logPath,mode='w+')
# handler.setLevel(logging.INFO)
# formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
# handler.setFormatter(formatter)
# logger.addHandler(handler)

print("SOCIALSIM DECEMBER CHALLENGE: SIMULATION")
print("VERSION: %s" % version_tag)
print("------------ platform: %s. domain: %s. version: %s"%(platform,domain,version_tag))





### reset simulation output dirs.
output_dir="./output/%s/%s/scenario_%d/%s/"%(platform,domain,scenario,version_tag)
reset_dir(output_dir)
print("[reset] output dir: %s"%output_dir)


## loading prob. distributions
print("Loading probability distributions..")

print("[Degree by level] loading..")     
data_level_degree_list=pd.read_pickle("./metadata/probs_v1/%s-%s/degree_cond_level_infoid.pkl.gz"%(platform,domain))

print("[Delay sequences by size] loading..")
data_delay_level_degree_root_list=pd.read_pickle("./metadata/probs_v1/%s-%s/delay_cond_size_infoid.pkl.gz"%(platform,domain))


print("[User probability] loading..")
data_user=pd.read_pickle("./metadata/probs_v1/%s-%s/user_diffusion_all.pkl.gz"%(platform,domain))

        

## loading inout seeds
print("[input posts] Reading..")
iposts=_get_input_posts(input_posts_file_location)
print("[input posts] Done, # posts: %d"%iposts.shape[0])

## you can remove this in the challenge
##iposts=iposts.loc['2017-08-01':'2017-08-31']
###iposts=iposts.sample(20,random_state=42)
## print("[input posts] Done, # posts: %d"%iposts.shape[0])
####

iposts_records=iposts.to_dict(orient='records')

Parallel(n_jobs=8)(delayed(_run)([trial]) for trial in range(no_trials))
##_run([1])

    


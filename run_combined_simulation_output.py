import pandas as pd
import argparse
import numpy as np

from libs.pnnl_load import *

def write_output(output):
        output_location="./output/scenario_%d_id_%s.txt"%(scenario,version_tag)
        output_file = open(output_location, 'w', encoding='utf-8')
        identifier=version_tag
        header={"identifier": identifier, "team": "usf", "scenario": str(scenario)}
        json.dump(header, output_file) 
        output_file.write("\n")
        output_records=output.to_dict('records')
        for d in output_records:
            output_file.write(json.dumps(d) + '\n')
        print("Saved at: %s"%output_location)

"""
Load the simulation parameters
"""
parser = argparse.ArgumentParser(description='Simulation Parameters')
parser.add_argument('--config', dest='config_file_path', type=argparse.FileType('r'))
args = parser.parse_args()

config_json=json.load(args.config_file_path)
scenario = int(config_json["SCENARIO"])
version_tag = config_json["VERSION_TAG"]
simulation_paths = config_json["SIMULATION"]
informationID_path = config_json["INFORMATION_IDS"]

informationIDs = pd.read_csv(informationID_path,header=None)
informationIDs.columns=['informationID']
print("# infoIDs to simulate: %d"%informationIDs.shape[0])

sim_output_queue=[]
for domain,platform_values in simulation_paths.items():
    
    for platform,platform_path in platform_values.items():
        if platform_path:
            sim_output=load_data(platform_path)
            sim_output=pd.merge(sim_output,informationIDs,on='informationID',how='inner')
            print(platform,platform_path,sim_output.shape[0])
            sim_output_queue.append(sim_output)
            
        
sim_output=pd.concat(sim_output_queue)
print(sim_output['platform'].unique())
print(sim_output['informationID'].unique())
##sim_output=pd.merge(sim_output,informationIDs,on='informationID',how='inner')
sim_output["nodeTime"]=sim_output["nodeTime"].values.astype(np.int64) // 10 ** 9
write_output(sim_output)




# Conditional Branching Cascade Model

** Description: Cascade Generation Branching model conditioned by level, and infoID. Delay distribution by size. Works given an initial seed file.

## How to prepare the environment?

First, you need to crate `pnnl_socialsim` conda environment according to the specification at [PNNL GitHub page](https://github.com/pnnl/socialsim_package). 
While this environment gives you all necessary libraries, you might require few others. please perform a `conda install <lib>` <lib> is the required library if you come across an issue when running the code.


## How to prepare the resources?
This code require many helper files. They are in the form of probability distributions, configuration files, and initial seeds jsons. 

### Probability Distributions
1.  Degree distribution conditioned by level and informationID
2.  Delay distribution conditioned by cascade size
3.  User Diffusion Strength

These files do have standard names, and they are grouped in specific folders. Please see the following organization;
![probability distributions](https://drive.google.com/file/d/1SxeUCaoE242oEk5vdbG5Uwfir8hLKK14/view)


### Initial Seed Files
Initial seeds are in the form of original tweets or original posts.
![input seed format](https://drive.google.com/open?id=1d-Z7DL1_xKmaPVCme46z3WDssJ-jtci8)
A seed file is a json document, where each row in the json in the format as following:
```javascript
{"platform": "reddit", "nodeTime": "2017-07-15 19:53:00", "nodeUserID": "onbeLIcRrGEyEeHVa_mUVA", "communityID": "t5_2qioo", "informationID": "firefox", "nodeID": "t3_n3FK_3_bj3YX2u4z56oCDQ"}
```


### Configuration Files
Configuration file is the place that you specify the simulation parameters, such that **you never touch the actual code to do any modifications.**
A configuration file is a JSON document, and follow this format in an example of running twitter/ cve seed files:
```javascript
{
"VERSION_TAG":"branching_cve_v1",//you can have any name that describe your simulation
"PLATFORM":"twitter",// platform name
"ACTION_TYPE":"retweet",// action type, you can only input one action type per one simulation
"DOMAIN":"cve",// domain name
"SCENARIO":1,
"NO_TRIALS":10,// number of simulations
"INPUT_SEEDS_FILE_PATH":"./input/cve/sample_tweet_initd.json"// input seed file
}
```

## How to Run?
```
source activate pnnl_socialsim
```
This will activate the `pnnl_socialsim` conda environment.

```
python run_simulation_prob_branching_v1.py --config ./metadata/configs/sample.json
```
Please pass any configuration json path as the input config parameter.

## How to see the simulation output?
According to the `version_tag` that you specified in the confog json, you will see `output/scenario_<scenario_no>/<version_tag>` folder where all simulation output files are saved.
They are in ready-made format for any PNNL submission.

## Notes
You have to be sure that the names provided in the `PLATFORM` and `DOMAIN` do exist in the probability files. As an example, `twitter-cve` should exist in the probability distro folder with necessary probability files.


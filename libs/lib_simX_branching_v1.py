import numpy as np
import pandas as pd
import sys,os
from random import choices
import random
from datetime import datetime as dt
import json
from ast import literal_eval
import time

#from joblib import Parallel, delayed
from libs.lib_job_thread import *

import logging

class SimX:
    def __init__(self,*args):
        self.platform=args[0]
        self.actionType=args[1]
        self.domain=args[2]
        self.scenario=args[3]
        self.version_tag=args[4]
       
        self.pool=ThreadPool(32)
        self.sim_outputs=[]
        
        self.data_level_infoID_degree_list=None
        self.data_delay_level_infoID_degree_root_list=None
        
        self.data_user_list=None
        self.data_acts_list=None
        
        self.data_level_content_list=None
        
#         self.logger = logging.getLogger(__name__)
#         logPath='./logs/run_simulation_prob_HYDRA_%s_%s_S20001.log'%(self.platform,self.domain)
#         handler = logging.FileHandler(logPath,mode='w+')
#         handler.setLevel(logging.INFO)
#         formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
#         handler.setFormatter(formatter)
#         self.logger.addHandler(handler)
        
    def set_metadata(self,degree_list,delay_list):
        self.data_level_infoID_degree_list=degree_list
        self.data_delay_level_infoID_degree_root_list=delay_list
        
        
    def set_user_metadata(self,user_list):
        self.data_user_list=user_list
        self.data_acts_list=self.data_user_list[['pnodeUserID','total_no_responses']].drop_duplicates()
        ###self.data_user_list.set_index("target_author",inplace=True)
        
    def set_simulation_metadata(self,content_list):
        self.data_level_content_list=content_list
        
#     def doSanity(self):
#         # ## Given any level, return a node with degree X
#         level=200
#         b = self._get_degree(level)
#         print("[sanity] Level: %d, Sampled Degree: %d"%(level,b))


#         ## Given any size of the cascade, return a vector of delays
#         size=10000
#         dV = self._get_recorrected_delayV(size)
#         print("[sanity] Expected: %d, Returned: %d Sampled Delay Vector: "%(size,dV.shape[0]),dV)

#         # ## Given any degree in the first level, return an arbitrary cascade tree
#         root_degree=3
#         ctree=self._gen_cascade_tree(root_degree)
#         print("[sanity] generated cascade tree")
#         print(ctree)
        
    def _get_random_id(self):
        hash = random.getrandbits(64)
        return "%16x"%hash
    
    def _get_random_user_id(self):
        try:
            random_user_id=self.data_acts_list.sample(n=1,weights="total_no_responses",replace=True).iloc[0]["pnodeUserID"]
        except KeyError as ke:
            random_user_id=self._get_random_id()
            ##print("new user: ",random_user_id)
            
        return random_user_id
    
    def _get_neighbor_user_id(self,user_id):
        try:
            ###random_user_id=self.data_user_list.loc[user_id].sample(n=1,weights="prob",replace=True).iloc[0]["source_author"]
            ##print(self.data_user_list[self.data_user_list['target_author']==user_id])
            neighbors=self.data_user_list[self.data_user_list['pnodeUserID']==user_id]
            if neighbors.shape[0]>0:
                random_user_id=neighbors.sample(n=1,weights="prob",replace=True).iloc[0]["nodeUserID"]
            else:
                random_user_id=self._get_random_user_id()
        except:
            random_user_id=self._get_random_user_id()
        return random_user_id

    def _get_random_users(self,size):
        return [self._get_random_id() for i in range(size)]
    
#     def write_output(self,output,scenario,platform,domain,version):
#         scenario=str(scenario)
#         version=str(version)
#         submission = {'team'     : "usf",
#                       'scenario' : scenario,
#                       'domain'   : domain,
#                       'platform' : platform,
#                       'data'     : literal_eval(output.to_json(orient='records'))}

#         output_location="./output/%s/%s/scenario_%s/scenario_%s_domain_%s-%s_v%s.json"%(platform,domain,scenario,scenario,platform,domain,version)
#         with open(output_location,'w') as f:
#             json.dump(submission, f)
            
    def write_output(self,output,version):
        version=str(version)
        output_location="./output/%s/%s/scenario_%d/%s/scenario_%d_domain_%s-%s_v%s.txt"%(self.platform,self.domain,self.scenario,self.version_tag,self.scenario,self.platform,self.domain,version)
        output_file = open(output_location, 'w', encoding='utf-8')
        identifier=self.version_tag+"_"+str(version)
        header={"identifier": identifier, "team": "usf", "scenario": str(self.scenario)}
        json.dump(header, output_file) 
        output_file.write("\n")

        output_records=output.to_dict('records')
        for d in output_records:
            output_file.write(json.dumps(d) + '\n')
    
        
    def _get_degree(self,level,informationID):
        try:
            infoID_slice=self.data_level_infoID_degree_list.loc[informationID]
        except KeyError as ke:
            all_infoIDs=list(set(self.data_level_infoID_degree_list.index.get_level_values(0)))
            informationID=np.random.choice(all_infoIDs)
            print('Random group selection: %s'%informationID)
            infoID_slice=self.data_level_infoID_degree_list.loc[informationID]

        ulevels=set(infoID_slice.index.get_level_values('level'))
        flag=False;
        while not flag:
            flag=(level in ulevels)
            if(flag==False):
                level-=1
#         print('Level selection: %d'%level)
        sampled_degree = np.random.choice(a=infoID_slice.loc[level]['udegreeV'], p=infoID_slice.loc[level]["probV"])
        return sampled_degree

    def _get_delayV(self,size,informationID):
        try:
            infoID_slice=self.data_delay_level_infoID_degree_root_list.loc[informationID]
        except KeyError as ke:
            all_infoIDs=list(set(self.data_delay_level_infoID_degree_root_list.index.get_level_values(0)))
            informationID=np.random.choice(all_infoIDs)
#             print('Random group selection: %s'%informationID)
            infoID_slice=self.data_delay_level_infoID_degree_root_list.loc[informationID]

        sample_delays=infoID_slice[infoID_slice["size"]==size]
        no_records=0
        dV=pd.Series()
        try:
#             print(sample_delays)
            no_records=sample_delays.shape[0]
        except:
            dV=pd.Series([pd.Timedelta(days=0, minutes=np.random.choice(60))])
            dV=np.array(list(dV))
            return dV

        if no_records>0:
            sample_delay=sample_delays.sample(n=1, replace=False)
            dV=np.array(list(sample_delay["delayV"]))[0]

            return dV

        else:
            max_size=infoID_slice["size"].max()
            if(size>max_size):
                return self._get_delayV(max_size,informationID)
            else:
                return self._get_delayV(size+1,informationID)
            
    

    def _get_recorrected_delayV(self,size,informationID):
        dV = self._get_delayV(size,informationID)
        
        
        if(dV.shape[0]>size):
            dV=dV[:size]
        else:
            max_ldelay=dV.max()
            for n in range(len(dV), size):
                max_ldelay+=pd.Timedelta(days=0, minutes=np.random.choice(60), seconds=np.random.choice(60))
                dV=np.append(dV,max_ldelay)
                
        bits=[pd.Timedelta(days=0, minutes=np.random.choice(30), seconds=np.random.choice(60)) for _ in range(len(dV))]
        dV=[x + y for x, y in zip(dV, bits)]
        return dV
    
#     def _get_contentV(self,level):
#         ulevels=set(self.data_level_content_list.index.get_level_values('level'))
#         flag=False;
#         while not flag:
#             flag=(level in ulevels)
#             if(flag==False):
#                 level-=1
#         contentV=self.data_level_content_list.iloc[level]['contentV']
#         sampled_contentV = contentV[np.random.randint(0,len(contentV))]
#         return sampled_contentV[1:]


    def _get_synthetic_tree_recursive(self,level,pdegree,informationID,cascade_tree_matrix,nlist):

        if(cascade_tree_matrix is None):
            cascade_tree_matrix=[]
            cascade_tree_matrix.append(nlist)

        children=pdegree

        while(children>0):
            mid=self._get_random_id()
            pid=nlist[2]
            puser_id=nlist[4]
            
            ndegree=self._get_degree(level,informationID)
            nuser_id=self._get_neighbor_user_id(puser_id)
            ###nuser_id=self._get_random_id()
            
            klist=[level,ndegree,mid,pid,nuser_id,informationID]

            cascade_tree_matrix.append(klist)
            self._get_synthetic_tree_recursive(level+1,ndegree,informationID,cascade_tree_matrix,klist)
            children-=1

        return cascade_tree_matrix

    def _gen_cascade_tree(self,informationID,pid=None,puser_id=None,pdegree=None):
        level=0
        
        ## post id
        if pid is None:
            pid=self._get_random_id()
        ## post user id 
        if puser_id is None:
            puser_id=self._get_random_user_id()
        ## post degree
        if pdegree is None:
            pdegree=self._get_degree(level,informationID)
            
        ## level, my degree, my id, my parent id
        nlist=[level,pdegree,pid,pid,puser_id,informationID]
        cascade_tree_matrix=self._get_synthetic_tree_recursive(level+1,pdegree,informationID,None,nlist)
        cascade_tree=pd.DataFrame(cascade_tree_matrix,columns=["level","degree","nodeID","parentID","nodeUserID","informationID"])
        cascade_tree["rootID"]=pid
        cascade_tree["actionType"]=self.actionType
        cascade_tree.loc[:0,"actionType"] =self.actionType
        cascade_tree.sort_values(by='level',inplace=True)

        ## attach the delays
        ctree_size=cascade_tree.shape[0]
        
        recorrected_delayV=self._get_recorrected_delayV(ctree_size,informationID)
        cascade_tree["long_propagation_delay"]=np.sort(recorrected_delayV)
        return cascade_tree


    def _simulate(self,ipost):
        
        ipost_id=ipost['id_h']
        ipost_user=ipost['author_h']
        ipost_degree=None#ipost['degree']
        ipost_created_date=str(ipost['created_date'])
        ipost_informationID=ipost['informationID']
        ipost_communityID=ipost['communityID']

        ##print("started: ",start)
        print("[simulation] informationID: %s, communityID: %s, post id: %s, author: %s, timestamp: %s"%(ipost_informationID,ipost_communityID,ipost_id,ipost_user,ipost_created_date))

        ipost_tree=self._gen_cascade_tree(ipost_informationID,ipost_id,ipost_user,ipost_degree)
        
        ##print("-",ipost_tree)
        # change the post id
#         assigned_rootID=ipost_tree['rootID'].iloc[0]
#         ipost_tree.replace({assigned_rootID: ipost_id}, regex=True,inplace=True)

        # assign times
        ipost_tree["nodeTime"]=ipost_created_date
        ipost_tree["nodeTime"]=pd.to_datetime(ipost_tree["nodeTime"])
        ipost_tree["nodeTime"]+=ipost_tree["long_propagation_delay"]

#         # assign authors
#         ipost_tree_size=ipost_tree.shape[0]
#         ## random users
#         ipost_tree["nodeUserID"]=self._get_random_users(ipost_tree_size)
#         ## fix the poster
#         ipost_tree.loc[:0,"nodeUserID"]=ipost_user


        icols=["nodeID","nodeUserID","parentID", "rootID", "actionType", "nodeTime","informationID"]
        ipost_tree=ipost_tree[icols]

        ## change to timestamp
        ipost_tree["nodeTime"]=ipost_tree["nodeTime"].values.astype(np.int64) // 10 ** 9
        
#         ipost_tree["informationID"]=ipost_group
        ipost_tree["platform"]=self.platform

#         ## assign communityID
        ipost_tree["communityID"]=ipost_communityID
        ##print("--",ipost_tree)
        self.sim_outputs.append(ipost_tree)
        return ipost_tree
    
    def _run_simulate(self,iposts_records,trial):
        start = time.time()
        for ipost in iposts_records:
#             self._simulate(ipost)
#             break;
            self.pool.add_task(self._simulate,ipost)
        self.pool.wait_completion()
        end = time.time()
        elapsed=end - start
        
        
        sim_output=pd.concat(self.sim_outputs)
        
        no_cascades=len(self.sim_outputs)
        no_acts=sim_output.shape[0]
        print("[simulation completed] version: %d, # cascades: %d, # acts: %d, Elapsed %.3f seconds."%(trial, no_cascades,no_acts,elapsed))


        self.write_output(sim_output,trial)
        